# App Building phase --------
FROM openjdk:11.0.16-jdk-slim@sha256:868a4f2151d38ba6a09870cec584346a5edc8e9b71fde275eb2e0625273e2fd8 AS build

RUN mkdir /appbuild
COPY . /appbuild

WORKDIR /appbuild

RUN ./gradlew clean installDist
# End App Building phase --------

# Container setup --------
FROM openjdk:11.0.16-jre-slim@sha256:93af7df2308c5141a751c4830e6b6c5717db102b3b31f012ea29d842dc4f2b02

ENV APPLICATION_USER appuser
RUN useradd -u 1033 $APPLICATION_USER && mkdir /app && chown -R $APPLICATION_USER /app && chmod -R 755 /app
USER $APPLICATION_USER

WORKDIR /app
EXPOSE 8080
CMD ["sh", "-c", "./bin/web"]

COPY --from=build /appbuild/web/build/install/web/ /app/
# End Container setup --------
