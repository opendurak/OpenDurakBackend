package com.opendurak.opendurakbackend.domain.game.model

import com.opendurak.opendurakbackend.domain.lobby.model.UserId

internal data class Player internal constructor(
    val id: UserId,
    val index: Int,
    val cards: MutableList<Card>,
)