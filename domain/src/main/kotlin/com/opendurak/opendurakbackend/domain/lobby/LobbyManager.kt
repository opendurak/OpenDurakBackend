package com.opendurak.opendurakbackend.domain.lobby

import java.util.concurrent.ConcurrentHashMap

public class LobbyManager(
    private val lobbyFactory: () -> Lobby,
    private val uuidFactory: () -> String,
) {
    private val lobbies = ConcurrentHashMap<String, Lobby>()

    public fun getAllLobbies(): List<Lobby> = lobbies.values.toList()

    public fun findLobby(lobbyId: String): Lobby? = lobbies[lobbyId]

    public fun createNewLobby(): Lobby {
        val lobby = lobbyFactory.invoke()
        var lobbyId: String
        do {
            lobbyId = uuidFactory.invoke()
            lobby.lobbyId = lobbyId
            val existingLobbyWithSameId = lobbies.putIfAbsent(lobbyId, lobby)
        } while (existingLobbyWithSameId != null)
        return lobby
    }
}