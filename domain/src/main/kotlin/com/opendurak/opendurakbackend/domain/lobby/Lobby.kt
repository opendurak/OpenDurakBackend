package com.opendurak.opendurakbackend.domain.lobby

import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.lobby.interactor.SendPersonalGameStateUseCase
import com.opendurak.opendurakbackend.domain.lobby.model.LobbyId
import com.opendurak.opendurakbackend.domain.lobby.model.UserId
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.util.concurrent.ConcurrentHashMap

public class Lobby internal constructor(
    private val sendPersonalGameStateUseCaseFactory: (Lobby) -> SendPersonalGameStateUseCase,
    private val uuidFactory: () -> String,
) {
    public lateinit var lobbyId: LobbyId
        internal set

    internal val gameMutex = Mutex()

    @Volatile
    internal var game: Game? = null

    private val connectedUsers = ConcurrentHashMap<UserId, User>()
    internal val users: Map<UserId, User> = connectedUsers

    public suspend fun joinLobby(userName: String): UserId {
        val user = User(userName, null, null)
        val userId = addUserToLobbyWithNewUserId(user)
        broadcastUsersInLobby()
        return userId
    }

    private fun addUserToLobbyWithNewUserId(user: User): UserId {
        var userId: UserId
        do {
            userId = uuidFactory.invoke()
            val existingUserWithSameId = connectedUsers.putIfAbsent(userId, user)
        } while (existingUserWithSameId != null)
        return userId
    }

    private suspend fun broadcastUsersInLobby() {
        val allLobbyMembers = users.values.map { it.userName }
        users.values.mapNotNull { it.lobbyCommunicationChannel }.forEach {
            it.send(allLobbyMembers)
        }
    }

    /**
     * User subscribes lobby and overrides subscription to lobby updates with to the new channel.
     * @throws IllegalArgumentException if user did not join lobby before
     */
    @Throws(IllegalArgumentException::class)
    public suspend fun subscribeLobby(userId: UserId, lobbyChannel: CommunicationChannel) {
        val user = connectedUsers[userId]
        if (user != null) {
            user.lobbyCommunicationChannel = lobbyChannel
            val allLobbyMembers = users.values.map { it.userName }
            lobbyChannel.send(allLobbyMembers)
        } else {
            throw IllegalArgumentException("User doesnt exist")
        }
    }

    /**
     * Unsubscribes the user to lobby updates if he is subscribed with the same [lobbyChannel].
     * The user does not leave the lobby.
     */
    public fun unsubscribeToLobby(userId: UserId, lobbyChannel: CommunicationChannel) {
        val user = connectedUsers[userId]
        if (user != null && user.lobbyCommunicationChannel == lobbyChannel)
            user.lobbyCommunicationChannel = null
    }

    /**
     * @throws IllegalArgumentException if user did not join lobby before
     */
    @Throws(IllegalArgumentException::class)
    public suspend fun subscribeToGameState(userId: UserId, gameStateChannel: CommunicationChannel) {
        val user = connectedUsers[userId]
        if (user != null) {
            user.gameStateCommunicationChannel = gameStateChannel

            gameMutex.withLock {
                sendPersonalGameStateUseCaseFactory.invoke(this).sendToUser(userId)
            }
        } else {
            throw IllegalArgumentException("User is not part of Lobby. Cannot subscribe game state")
        }
    }

    public fun unsubscribeToGameState(userId: UserId, gameStateChannel: CommunicationChannel) {
        val user = connectedUsers[userId]
        if (user != null && user.gameStateCommunicationChannel == gameStateChannel)
            user.gameStateCommunicationChannel = null
    }

    public data class User internal constructor(
        internal val userName: String,
        internal var lobbyCommunicationChannel: CommunicationChannel?,
        internal var gameStateCommunicationChannel: CommunicationChannel?,
    )
}