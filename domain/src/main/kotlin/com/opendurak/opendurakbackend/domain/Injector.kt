package com.opendurak.opendurakbackend.domain

import com.opendurak.opendurakbackend.domain.game.GameApiFacade
import com.opendurak.opendurakbackend.domain.game.GameApiFacadeImpl
import com.opendurak.opendurakbackend.domain.game.interactors.*
import com.opendurak.opendurakbackend.domain.lobby.Lobby
import com.opendurak.opendurakbackend.domain.lobby.LobbyManager
import com.opendurak.opendurakbackend.domain.lobby.interactor.HashUserIdUseCase
import com.opendurak.opendurakbackend.domain.lobby.interactor.SendPersonalGameStateUseCase
import java.security.MessageDigest
import java.util.*

public class Injector(
    messageDigest: MessageDigest,
) {

    private val doesCardBeatUseCase = DoesCardBeatUseCase()
    private val giveUpUseCase = GiveUpUseCase()
    private val helpOnBattlefieldUseCase = HelpOnBattlefieldUseCase()
    private val defendOnBattlefieldUseCase = DefendOnBattlefieldUseCase(doesCardBeatUseCase)
    private val attackOnBattlefieldUseCase = AttackOnBattlefieldUseCase()
    private val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
    private val createGameUseCase = CreateGameUseCase(fillHandTo6CardsUseCase)
    private val progressRoundUseCase = ProgressRoundUseCase(fillHandTo6CardsUseCase)

    private val hashUserIdUseCase = HashUserIdUseCase(messageDigest)
    private val sendPersonalGameStateUseCaseFactory =
        { lobby: Lobby -> SendPersonalGameStateUseCase(lobby, hashUserIdUseCase) }

    private val uuidFactory = { UUID.randomUUID().toString() }

    public val lobbyManager: LobbyManager =
        LobbyManager({ Lobby(sendPersonalGameStateUseCaseFactory, uuidFactory) }, uuidFactory)

    public val gameApiFacade: GameApiFacade = GameApiFacadeImpl(lobbyManager,
        createGameUseCase, attackOnBattlefieldUseCase, defendOnBattlefieldUseCase,
        giveUpUseCase, helpOnBattlefieldUseCase, sendPersonalGameStateUseCaseFactory, progressRoundUseCase)
}
