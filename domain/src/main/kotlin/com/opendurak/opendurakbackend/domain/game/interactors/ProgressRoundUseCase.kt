package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Game

internal class ProgressRoundUseCase(
    private val fillHandTo6CardsUseCase: FillHandTo6CardsUseCase,
) {

    fun execute(game: Game) {
        val oneSideGaveUp = didOffendersGiveUp(game) || game.defenderGaveUp
        val isBattlefieldFinished = isBattlefieldFull(game.battlefield) || game.defender.cards.isEmpty()

        if (oneSideGaveUp || isBattlefieldFinished) finishRound(game)
    }

    private fun finishRound(game: Game) {
        val didDefenderWin = game.battlefield.areAllCardsDefended()
        cleanUpRound(didDefenderWin, game)
        if (game.players.size >= 2) rotatePlayerRoles(didDefenderWin, game)
        else game.gameFinished = true
    }

    private fun cleanUpRound(didDefenderWin: Boolean, game: Game) {
        clearBattlefield(!didDefenderWin, game)
        fillPlayerCards(game)
        resetGiveUpState(game)
        game.players.removeAll { it.cards.isEmpty() }
    }

    private fun rotatePlayerRoles(didDefenderWin: Boolean, game: Game) {
        game.attacker =
            if (didDefenderWin) getNextPlayerAfter(game.attacker.index, game)
            else getNextPlayerAfter(game.defender.index, game)
        game.defender = getNextPlayerAfter(game.attacker.index, game)
        game.helper = if (game.players.size == 2) null else getNextPlayerAfter(game.defender.index, game)
    }

    private fun getNextPlayerAfter(afterId: Int, game: Game) =
        game.players.firstOrNull { it.index > afterId } ?: game.players.first()

    private fun clearBattlefield(didDefenderLoose: Boolean, game: Game) {
        if (didDefenderLoose) game.defender.cards.addAll(game.battlefield.getAllCards())
        game.battlefield.clearBattlefield()
    }

    private fun resetGiveUpState(game: Game) {
        game.attackerGaveUp = false
        game.defenderGaveUp = false
        game.helperGaveUp = false
    }

    private fun fillPlayerCards(game: Game) {
        fillHandTo6CardsUseCase.execute(game.attacker, game.stack)
        fillHandTo6CardsUseCase.execute(game.defender, game.stack)
        val helper = game.helper
        if (helper != null) {
            fillHandTo6CardsUseCase.execute(helper, game.stack)
        }
    }

    private fun didOffendersGiveUp(game: Game) =
        game.attackerGaveUp && (game.helperGaveUp || game.helper == null)

    private fun isBattlefieldFull(battlefield: Battlefield) =
        battlefield.getNumberOfStacks() == 6 && battlefield.areAllCardsDefended()
}