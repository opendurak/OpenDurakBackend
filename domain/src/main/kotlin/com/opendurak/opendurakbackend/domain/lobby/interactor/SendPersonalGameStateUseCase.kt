package com.opendurak.opendurakbackend.domain.lobby.interactor

import com.opendurak.opendurakbackend.domain.lobby.Lobby
import com.opendurak.opendurakbackend.domain.lobby.model.GameState
import com.opendurak.opendurakbackend.domain.lobby.model.HashedPlayer
import com.opendurak.opendurakbackend.domain.lobby.model.HiddenPlayer
import com.opendurak.opendurakbackend.domain.lobby.model.UserId

/**
 * Send the Game State personalized for the user.
 *
 * A player can only see his and open cards.
 * If the user is not a player he can only see open cards. (= spectator).
 *
 * The user ids are hashed so users dont know other users id.
 *
 * On creation the game is analysed. Then the personalized result can be sent to specific players or the whole lobby.
 */
internal class SendPersonalGameStateUseCase(
    private val lobby: Lobby,
    private val hashUserIdUseCase: HashUserIdUseCase,
) {
    private val game = lobby.game

    private val hiddenPlayers = game?.players?.map {
        HiddenPlayer(
            hashUserIdUseCase.hash(it.id),
            lobby.users[it.id]?.userName ?: "unknown",
            it.cards.count()
        )
    }

    suspend fun broadcastToLobby() {
        lobby.users.forEach { receiver ->
            val gameStateCommunicationChannel = receiver.value.gameStateCommunicationChannel
            if (gameStateCommunicationChannel != null) {
                val gameState = getPersonalGameState(receiver.key)
                gameStateCommunicationChannel.send(gameState)
            }
        }
    }

    @Throws(NullPointerException::class)
    suspend fun sendToUser(userId: UserId) {
        val gameState = getPersonalGameState(userId)
        lobby.users[userId]!!.gameStateCommunicationChannel!!.send(gameState)
    }

    private fun getPersonalGameState(userId: UserId): GameState? {
        if (game != null) {
            val player = game.players.find { it.id == userId }?.let {
                HashedPlayer(hashUserIdUseCase.hash(it.id), it.cards)
            }

            return GameState(
                player, hiddenPlayers!!, game.stack.count(), game.trump,
                game.battlefield.map,
                hashUserIdUseCase.hash(game.attacker.id),
                hashUserIdUseCase.hash(game.defender.id),
                game.helper?.id?.let { hashUserIdUseCase.hash(it) },
                game.attackerGaveUp, game.defenderGaveUp, game.helperGaveUp,
                game.gameFinished
            )
        } else {
            return null
        }
    }

}