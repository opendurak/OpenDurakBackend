package com.opendurak.opendurakbackend.domain.lobby.model

public data class HiddenPlayer internal constructor(
    val hash: UserHash,
    val username: String,
    val cardCount: Int,
)