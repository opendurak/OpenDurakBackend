package com.opendurak.opendurakbackend.domain.game.model

public data class Game internal constructor(
    internal val players: MutableList<Player>,
    internal val stack: MutableList<Card>,
    internal val trump: Card,
    internal val battlefield: Battlefield,
    internal var attacker: Player,
    internal var defender: Player,
    internal var helper: Player?,
    internal var attackerGaveUp: Boolean,
    internal var defenderGaveUp: Boolean,
    internal var helperGaveUp: Boolean,
    internal var gameFinished: Boolean,
)