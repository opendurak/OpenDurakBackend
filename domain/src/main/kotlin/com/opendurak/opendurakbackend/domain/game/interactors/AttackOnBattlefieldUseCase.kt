package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player

internal class AttackOnBattlefieldUseCase {

    @Throws(IllegalArgumentException::class)
    fun execute(card: Card, player: Player, game: Game) {
        if (canPlayerAttackWithCard(card, player, game) && canCardBeAddedToBattlefield(card, game))
            attackWithCard(card, player, game.battlefield)
        else throw IllegalArgumentException("Cannot attack")
    }

    private fun canPlayerAttackWithCard(card: Card, player: Player, game: Game): Boolean {
        val isPlayerAttacker = player == game.attacker
        return isPlayerAttacker && player.cards.contains(card)
    }

    private fun canCardBeAddedToBattlefield(card: Card, game: Game): Boolean {
        return if (isNumberOfCardsInBattlefieldNotReached(game)) {
            if (game.battlefield.getAttackingCards().isEmpty()) true
            else isCardValueInBattlefield(card, game.battlefield)
        } else false
    }

    private fun isNumberOfCardsInBattlefieldNotReached(game: Game): Boolean {
        val lessCardsInBattlefieldThanInDefendersHand =
            game.battlefield.getNumberOfCardsNotDefended() < game.defender.cards.size
        val lessThan6CardsInBattlefield = game.battlefield.getNumberOfStacks() < 6
        return lessCardsInBattlefieldThanInDefendersHand && lessThan6CardsInBattlefield
    }

    private fun isCardValueInBattlefield(card: Card, battlefield: Battlefield): Boolean {
        return battlefield.getAllCards().any { it.value == card.value }
    }

    private fun attackWithCard(card: Card, player: Player, battlefield: Battlefield) {
        player.cards.remove(card)
        battlefield.addAttackingCard(card)
    }
}