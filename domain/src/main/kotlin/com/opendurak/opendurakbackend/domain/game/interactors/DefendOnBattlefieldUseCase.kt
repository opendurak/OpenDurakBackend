package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player

internal class DefendOnBattlefieldUseCase(
    private val doesCardBeatUseCase: DoesCardBeatUseCase,
) {

    @Throws(IllegalArgumentException::class)
    fun execute(defenderCard: Card, attackerCard: Card, player: Player, game: Game) {
        if (canPlayerDefendWithCard(player, defenderCard, game)
            && isCardActiveAttacker(attackerCard, game.battlefield)
            && doesCardBeatUseCase.execute(defenderCard, attackerCard, game.trump)
        ) defend(defenderCard, attackerCard, player, game.battlefield)
        else throw IllegalArgumentException("Cannot defend")
    }

    private fun canPlayerDefendWithCard(
        player: Player, defenderCard: Card, game: Game,
    ): Boolean {
        return player == game.defender && player.cards.contains(defenderCard)
    }

    private fun isCardActiveAttacker(attackerCard: Card, battlefield: Battlefield): Boolean {
        return if (battlefield.getAttackingCards().contains(attackerCard)) {
            battlefield.getDefendingCard(attackerCard) == null
        } else false
    }

    @Throws(IllegalArgumentException::class)
    private fun defend(defenderCard: Card, attackerCard: Card, player: Player, battlefield: Battlefield) {
        player.cards.remove(defenderCard)
        battlefield.addDefendingCard(defenderCard, attackerCard)
    }

}