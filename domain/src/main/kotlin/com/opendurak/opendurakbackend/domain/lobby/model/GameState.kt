package com.opendurak.opendurakbackend.domain.lobby.model

import com.opendurak.opendurakbackend.domain.game.model.Card

public data class GameState internal constructor(
    val selfPlayer: HashedPlayer?,
    val players: List<HiddenPlayer>,
    val stackCount: Int,
    val trump: Card,
    val battlefield: Map<Card, Card?>,
    val attackerHash: UserHash,
    val defenderHash: UserHash,
    val helperHash: UserHash?,
    val attackGaveUp: Boolean,
    val defenderGaveUp: Boolean,
    val helperGaveUp: Boolean,
    val gameFinished: Boolean,
)