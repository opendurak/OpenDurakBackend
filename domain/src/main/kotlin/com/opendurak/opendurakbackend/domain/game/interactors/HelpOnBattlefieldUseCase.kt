package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player

internal class HelpOnBattlefieldUseCase {

    @Throws(IllegalArgumentException::class)
    fun execute(card: Card, player: Player, game: Game) {
        if (canPlayerHelpWithCard(card, player, game) && canCardBeAddedToBattlefield(card, game))
            helpWithCard(card, player, game)
        else throw IllegalArgumentException("Cannot help")
    }

    private fun canPlayerHelpWithCard(card: Card, player: Player, game: Game): Boolean {
        val isPlayerHelper = player == game.helper
        return isPlayerHelper && player.cards.contains(card)
    }

    private fun canCardBeAddedToBattlefield(card: Card, game: Game): Boolean {
        return if (isNumberOfCardsInBattlefieldNotReached(game)) {
            if (game.battlefield.getNumberOfStacks() == 0) false
            else isCardValueInBattlefield(card, game.battlefield)
        } else false
    }

    private fun isNumberOfCardsInBattlefieldNotReached(game: Game): Boolean {
        val lessCardsInBattlefieldThanInDefendersHand =
            game.battlefield.getNumberOfCardsNotDefended() < game.defender.cards.size
        val lessThan6CardsInBattlefield = game.battlefield.getNumberOfStacks() < 6
        return lessCardsInBattlefieldThanInDefendersHand && lessThan6CardsInBattlefield
    }

    private fun isCardValueInBattlefield(card: Card, battlefield: Battlefield): Boolean {
        return battlefield.getAllCards().any { it.value == card.value }
    }

    private fun helpWithCard(card: Card, player: Player, game: Game) {
        player.cards.remove(card)
        game.battlefield.addAttackingCard(card)
    }
}