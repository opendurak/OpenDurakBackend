package com.opendurak.opendurakbackend.domain.game

import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.lobby.Lobby
import com.opendurak.opendurakbackend.domain.lobby.model.LobbyId
import com.opendurak.opendurakbackend.domain.lobby.model.UserId

public interface GameApiFacade {
    @Throws(IllegalArgumentException::class)
    public suspend fun start(lobbyId: LobbyId, userId: UserId)

    public suspend fun attack(card: Card, lobbyId: LobbyId, userId: UserId)

    public suspend fun help(card: Card, lobbyId: LobbyId, userId: UserId)

    public suspend fun defend(defenderCard: Card, attackerCard: Card, lobbyId: LobbyId, userId: UserId)

    public suspend fun giveUp(lobbyId: LobbyId, userId: UserId)

    public fun getLobby(lobbyId: LobbyId): Lobby

    public fun requireUserIsInLobby(userId: UserId, lobby: Lobby)
}