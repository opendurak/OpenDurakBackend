package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Card
import org.junit.Test

class DoesCardBeatUseCaseTest {
    private val doesCardBeatUseCase = DoesCardBeatUseCase()

    @Test
    fun `when cardIsTrump execute should returnTrue`() {
        val actual = doesCardBeatUseCase.execute(
            Card(Card.Suit.DIAMOND, Card.Value.SIX),
            Card(Card.Suit.SPADE, Card.Value.KING),
            Card(Card.Suit.DIAMOND, Card.Value.EIGHT),
        )

        assertThat(actual).isTrue()
    }

    @Test
    fun `when cardIsDifferentSuit execute should returnFalse`() {
        val actual = doesCardBeatUseCase.execute(
            Card(Card.Suit.HEART, Card.Value.SIX),
            Card(Card.Suit.SPADE, Card.Value.KING),
            Card(Card.Suit.DIAMOND, Card.Value.EIGHT),
        )

        assertThat(actual).isFalse()
    }

    @Test
    fun `when cardIsDifferentSuitAndOtherIsTrump execute should returnFalse`() {
        val actual = doesCardBeatUseCase.execute(
            Card(Card.Suit.HEART, Card.Value.SIX),
            Card(Card.Suit.DIAMOND, Card.Value.KING),
            Card(Card.Suit.DIAMOND, Card.Value.EIGHT),
        )

        assertThat(actual).isFalse()
    }

    @Test
    fun `when cardHasLowerValue execute should returnFalse`() {
        val actual = doesCardBeatUseCase.execute(
            Card(Card.Suit.HEART, Card.Value.SIX),
            Card(Card.Suit.HEART, Card.Value.KING),
            Card(Card.Suit.DIAMOND, Card.Value.EIGHT),
        )

        assertThat(actual).isFalse()
    }

    @Test
    fun `when cardHasHigherValue execute should returnTrue`() {
        val actual = doesCardBeatUseCase.execute(
            Card(Card.Suit.HEART, Card.Value.KING),
            Card(Card.Suit.HEART, Card.Value.SIX),
            Card(Card.Suit.DIAMOND, Card.Value.EIGHT),
        )

        assertThat(actual).isTrue()
    }
}
