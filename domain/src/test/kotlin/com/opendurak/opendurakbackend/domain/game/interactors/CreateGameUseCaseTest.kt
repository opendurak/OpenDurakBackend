package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class CreateGameUseCaseTest {
    private val fillHandTo6CardsUseCase = mockk<FillHandTo6CardsUseCase>(relaxUnitFun = true)

    @Test(expected = IllegalArgumentException::class)
    fun `when noPlayers execute should throwException`() {
        CreateGameUseCase(fillHandTo6CardsUseCase).execute(emptyList())
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when onePlayer execute should throwException`() {
        CreateGameUseCase(fillHandTo6CardsUseCase).execute(listOf("a"))
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when sixPlayers execute should throwException`() {
        val tenPlayers = buildList { repeat(6) { add(it.toString()) } }
        CreateGameUseCase(fillHandTo6CardsUseCase).execute(tenPlayers)
    }

    @Test
    fun `execute should putAllCardsInStack`() {
        val players = buildList { repeat(3) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)
        assertThat(game.stack).hasSize(36)
    }

    @Test
    fun `execute should putLastCardOfStackAsTrump`() {
        val players = buildList { repeat(3) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)
        assertThat(game.trump).isEqualTo(game.stack.last())
    }

    @Test
    fun `when threePlayers execute should assignAttackerDefenderAndHelper`() {
        val players = buildList { repeat(3) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        assertThat(game.attacker).isNotNull()
        assertThat(game.defender).isNotNull()
        assertThat(game.helper).isNotNull()
    }

    @Test
    fun `when threePlayers execute should assignAttackerAndDefender`() {
        val players = buildList { repeat(3) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        assertThat(game.attacker).isNotNull()
        assertThat(game.defender).isNotNull()
    }

    @Test
    fun `when twoPlayers execute should notAssignHelper`() {
        val players = buildList { repeat(2) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        assertThat(game.helper).isNull()
    }

    @Test
    fun `execute should assignDifferentPlayersForAttackerDefenderAndHelper`() {
        val players = buildList { repeat(5) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        val allPlayersWithRole = listOf(game.attacker, game.defender, game.helper).map { it!!.id }
        assertThat(allPlayersWithRole).containsNoDuplicates()
    }

    @Test
    fun `execute should includeAllPlayers`() {
        val players = buildList { repeat(5) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        assertThat(game.players.map { it.id }).containsExactlyElementsIn(players)
    }

    @Test
    fun `execute should fillHandsForAllPlayers`() {
        val players = buildList { repeat(5) { add(it.toString()) } }
        val game = CreateGameUseCase(fillHandTo6CardsUseCase).execute(players)

        assertThat(game.players.map { it.id }).containsExactlyElementsIn(players)

        game.players.forEach {
            verify(exactly = 1) { fillHandTo6CardsUseCase.execute(it, game.stack) }
        }
    }

}