package com.opendurak.opendurakbackend.domain.game.model

import com.google.common.truth.Truth.assertThat
import io.mockk.mockk
import org.junit.Test

class BattlefieldTest {
    private val battlefield = Battlefield()

    @Test
    fun `clearBattlefield should removeAllCards`() {
        battlefield.addAttackingCard(mockk())
        battlefield.clearBattlefield()
        assertThat(battlefield.getAllCards()).hasSize(0)
    }

    @Test
    fun `addAttackingCard should addCard`() {
        battlefield.addAttackingCard(mockk())
        assertThat(battlefield.getAllCards()).hasSize(1)
    }

    @Test
    fun `addDefendingCard should addCard`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        assertThat(battlefield.getAllCards()).hasSize(2)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when attackingCardDoesntExist addDefendingCard should throwException`() {
        battlefield.addDefendingCard(mockk(), mockk())
    }

    @Test
    fun `when initialized areAllCardsDefended should returnTrue`() {
        assertThat(battlefield.areAllCardsDefended()).isTrue()
    }

    @Test
    fun `when attackingCardAdded areAllCardsDefended should returnFalse`() {
        battlefield.addAttackingCard(mockk())
        assertThat(battlefield.areAllCardsDefended()).isFalse()
    }

    @Test
    fun `when attackingCardDefended areAllCardsDefended should returnTrue`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        assertThat(battlefield.areAllCardsDefended()).isTrue()
    }

    @Test
    fun `when initialized getNumberOfStacks should return0`() {
        assertThat(battlefield.getNumberOfStacks()).isEqualTo(0)
    }

    @Test
    fun `when attackingCardDefended getNumberOfStacks should return1`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        assertThat(battlefield.getNumberOfStacks()).isEqualTo(1)
    }

    @Test
    fun `when attackingCardAdded getNumberOfStacks should return1`() {
        battlefield.addAttackingCard(mockk())
        assertThat(battlefield.getNumberOfStacks()).isEqualTo(1)
    }

    @Test
    fun `getDefendingCard should returnDefendingCard`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        val defendingCard = mockk<Card>()
        battlefield.addDefendingCard(defendingCard, attackingCard)
        assertThat(battlefield.getDefendingCard(attackingCard)).isEqualTo(defendingCard)
    }

    @Test
    fun `when attackingCardIsNotDefended getDefendingCard should returnNull`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        assertThat(battlefield.getDefendingCard(attackingCard)).isNull()
    }

    @Test
    fun `when attackingCardDoesntExist getDefendingCard should returnNull`() {
        assertThat(battlefield.getDefendingCard(mockk())).isNull()
    }
}