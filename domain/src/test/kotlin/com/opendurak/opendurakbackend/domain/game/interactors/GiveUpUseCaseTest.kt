package com.opendurak.opendurakbackend.domain.game.interactors

import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class GiveUpUseCaseTest {
    private val attacker = mockk<Player>()
    private val defender = mockk<Player>()
    private val helper = mockk<Player>()
    private val battlefield = Battlefield()
    private val game = mockk<Game>(relaxUnitFun = true) {
        every { this@mockk.attacker } returns this@GiveUpUseCaseTest.attacker
        every { this@mockk.defender } returns this@GiveUpUseCaseTest.defender
        every { this@mockk.helper } returns this@GiveUpUseCaseTest.helper
        every { this@mockk.battlefield } returns this@GiveUpUseCaseTest.battlefield
    }

    @Test
    fun `execute should letAttackerGiveUp`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        GiveUpUseCase().execute(attacker, game)
        verify(exactly = 1) { game.attackerGaveUp = true }
    }

    @Test
    fun `execute should letHelperGiveUp`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        GiveUpUseCase().execute(helper, game)
        verify(exactly = 1) { game.helperGaveUp = true }
    }

    @Test
    fun `execute should letDefenderGiveUp`() {
        battlefield.addAttackingCard(mockk())
        GiveUpUseCase().execute(defender, game)
        verify(exactly = 1) { game.defenderGaveUp = true }
    }

    @Test(expected = IllegalStateException::class)
    fun `when allCardsBeaten execute should notLetDefenderGiveUp`() {
        val attackingCard = mockk<Card>()
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(mockk(), attackingCard)
        GiveUpUseCase().execute(defender, game)
    }

    @Test(expected = IllegalStateException::class)
    fun `when cardsNotBeaten execute should notLetAttackerGiveUp`() {
        battlefield.addAttackingCard(mockk())
        GiveUpUseCase().execute(attacker, game)
    }

    @Test(expected = IllegalStateException::class)
    fun `when cardsNotBeaten execute should notLetHelperGiveUp`() {
        battlefield.addAttackingCard(mockk())
        GiveUpUseCase().execute(helper, game)
    }

    @Test(expected = IllegalStateException::class)
    fun `when noCardsPlayed execute should notLetAttackerGiveUp`() {
        GiveUpUseCase().execute(attacker, game)
    }

    @Test(expected = IllegalStateException::class)
    fun `when noCardsPlayed execute should notLetDefenderGiveUp`() {
        GiveUpUseCase().execute(defender, game)
    }

    @Test(expected = IllegalStateException::class)
    fun `when noCardsPlayed execute should notLetHelperGiveUp`() {
        GiveUpUseCase().execute(helper, game)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when unknownPlayer execute should throwException`() {
        GiveUpUseCase().execute(mockk(), game)
    }
}