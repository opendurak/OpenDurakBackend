package com.opendurak.opendurakbackend.domain.lobby.interactor

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import com.opendurak.opendurakbackend.domain.lobby.CommunicationChannel
import com.opendurak.opendurakbackend.domain.lobby.Lobby
import com.opendurak.opendurakbackend.domain.lobby.model.GameState
import com.opendurak.opendurakbackend.domain.lobby.model.HashedPlayer
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.Test
import java.util.Collections.singletonList

class SendPersonalGameStateUseCaseTest {
    private val channelMarc = createMockCommunicationChannel()
    private val channelMarckus = createMockCommunicationChannel()
    private val channelGerd = createMockCommunicationChannel()
    private val playerMarc = Player("69", 0, singletonList(Card(Card.Suit.SPADE, Card.Value.SIX)))
    private val playerMarckus = Player("53", 1, singletonList(Card(Card.Suit.DIAMOND, Card.Value.SIX)))

    private val game = mockk<Game>(relaxed = true) {
        every { players } returns mutableListOf(playerMarc, playerMarckus)
        every { attacker } returns playerMarc
        every { defender } returns playerMarckus
        every { helper } returns null
    }
    private val lobby = mockk<Lobby>(relaxed = true) {
        every { game } returns this@SendPersonalGameStateUseCaseTest.game
        every { users } returns mapOf(
            "69" to Lobby.User("Marc", null, channelMarc),
            "53" to Lobby.User("Marckus", null, channelMarckus),
            "42" to Lobby.User("Gerd", null, channelGerd),
        )
    }
    private val hashUserIdUseCase = mockk<HashUserIdUseCase> {
        every { hash(any()) } answers { "h${firstArg<String>()}" }
    }
    private val sendPersonalGameStateUseCase = SendPersonalGameStateUseCase(lobby, hashUserIdUseCase)

    @Test
    fun `when gameNotStarted it should sendNull`() {
        every { lobby.game } returns null

        val sendPersonalGameStateUseCase2 = SendPersonalGameStateUseCase(lobby, hashUserIdUseCase)
        runBlocking { sendPersonalGameStateUseCase2.sendToUser("69") }
        coVerify(exactly = 1) { channelMarc.send(null) }
    }

    @Test
    fun `sendToUser should onlySendToThatUser`() {
        runBlocking { sendPersonalGameStateUseCase.sendToUser("69") }

        coVerify(exactly = 0) { channelGerd.send(any<GameState>()) }
        coVerify(exactly = 0) { channelMarckus.send(any<GameState>()) }
        coVerify(exactly = 1) { channelMarc.send(any<GameState>()) }
    }

    @Test
    fun `when userIsPlayer sendToUser should returnCorrectSelfPlayer`() {
        val gameStateSlot = CapturingSlot<GameState>()
        coEvery { channelMarc.send(capture(gameStateSlot)) } just Runs
        runBlocking { sendPersonalGameStateUseCase.sendToUser("69") }
        assertThat(gameStateSlot.captured.selfPlayer).isEqualTo(
            HashedPlayer("h69", singletonList(Card(Card.Suit.SPADE, Card.Value.SIX)))
        )
    }

    @Test
    fun `when userIsSpectator sendToUser should returnNullAsSelfPlayer`() {
        val gameStateSlot = CapturingSlot<GameState>()
        coEvery { channelGerd.send(capture(gameStateSlot)) } just Runs
        runBlocking { sendPersonalGameStateUseCase.sendToUser("42") }
        assertThat(gameStateSlot.captured.selfPlayer).isNull()
    }

    @Test(expected = NullPointerException::class)
    fun `when userDoesntExist sendToUser should throwException`() {
        runBlocking { sendPersonalGameStateUseCase.sendToUser("100") }
    }

    @Test(expected = NullPointerException::class)
    fun `when gameStateChannelIsNull sendToUser should throwException`() {
        lobby.users["42"]!!.gameStateCommunicationChannel = null
        runBlocking { sendPersonalGameStateUseCase.sendToUser("42") }
    }

    @Test
    fun `broadcastToLobby should sendToAllUsers`() {
        runBlocking { sendPersonalGameStateUseCase.broadcastToLobby() }

        coVerify(exactly = 1) { channelGerd.send(any<GameState>()) }
        coVerify(exactly = 1) { channelMarckus.send(any<GameState>()) }
        coVerify(exactly = 1) { channelMarc.send(any<GameState>()) }
    }

    private fun createMockCommunicationChannel() = mockk<CommunicationChannel>(relaxUnitFun = true)
}