package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth
import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Card.Suit.*
import com.opendurak.opendurakbackend.domain.game.model.Card.Value.*
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.every
import io.mockk.mockk
import org.junit.Test

class HelpOnBattlefieldUseCaseTest {
    private val attacker = Player("Marvin", 0, mutableListOf(
        Card(SPADE, SIX)
    ))
    private val defender = Player("Alice", 1, mutableListOf(
        Card(SPADE, SEVEN)
    ))
    private val helper = Player("Bob", 2, mutableListOf(
        Card(SPADE, EIGHT)
    ))
    private val battlefield = Battlefield()
    private val game = mockk<Game> {
        every { this@mockk.attacker } returns this@HelpOnBattlefieldUseCaseTest.attacker
        every { this@mockk.defender } returns this@HelpOnBattlefieldUseCaseTest.defender
        every { this@mockk.helper } returns this@HelpOnBattlefieldUseCaseTest.helper
        every { this@mockk.battlefield } returns this@HelpOnBattlefieldUseCaseTest.battlefield
    }

    @Test
    fun `when battlefieldHasADefendedCardAndAttackerHasSameValue execute should removeCardFromHelper`() {
        val attackingCard = Card(DIAMOND, EIGHT)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(DIAMOND, SEVEN), attackingCard)
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
        Truth.assertThat(helper.cards).isEmpty()
    }

    @Test
    fun `when battlefieldHasADefendedCardAndAttackerHasSameValue execute should addCardToBattlefield`() {
        val attackingCard = Card(DIAMOND, EIGHT)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(DIAMOND, SEVEN), attackingCard)
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
        Truth.assertThat(battlefield.getAttackingCards()).hasSize(2)
    }

    @Test
    fun `when battlefieldHasADefendedCardAndDefenderHasSameValue execute should addCardToBattlefield`() {
        val attackingCard = Card(HEART, SEVEN)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(DIAMOND, EIGHT), attackingCard)
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
        Truth.assertThat(battlefield.getAttackingCards()).hasSize(2)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when emptyBattlefield execute should throwException`() {
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when playerUnknown execute should throwException`() {
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), mockk(), game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when defenderWantsToAttack execute should throwException`() {
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, SEVEN), defender, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when attackerDoesntHaveCard execute should throwException`() {
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, SEVEN), helper, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasSameNumberOfStacksAsDefender execute should throwException`() {
        battlefield.addAttackingCard(mockk())
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasCardsAndAttackingCardHasDifferentValue execute should throwException`() {
        val attackingCard = Card(HEART, SEVEN)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(HEART, SIX), attackingCard)
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasSixStacks execute should throwException`() {
        repeat(10) { defender.cards.add(mockk()) }
        repeat(6) { battlefield.addAttackingCard(mockk()) }
        HelpOnBattlefieldUseCase().execute(
            Card(SPADE, EIGHT), helper, game,
        )
    }
}