package com.opendurak.opendurakbackend.domain.lobby

import com.google.common.truth.Truth.assertThat
import io.mockk.*
import org.junit.Test

class LobbyManagerTest {

    @Test
    fun `when initialized getAllLobbies should returnEmpty`() {
        val lobbyManager = LobbyManager(
            { mockk() }, { "" }
        )
        assertThat(lobbyManager.getAllLobbies()).isEmpty()
    }

    @Test
    fun `when noLobby findLobby should returnNull`() {
        val lobbyManager = LobbyManager(
            { mockk() }, { "" }
        )
        assertThat(lobbyManager.findLobby("id")).isNull()
    }

    @Test
    fun `getNewLobby should assignUuid`() {
        val lobbyIdSlot = CapturingSlot<String>()
        val lobbyManager = LobbyManager(
            {
                mockk {
                    every { lobbyId = capture(lobbyIdSlot) } just Runs
                }
            },
            mockk {
                every { this@mockk.invoke() } returns "a"
            }
        )

        lobbyManager.createNewLobby()

        assertThat(lobbyIdSlot.captured).isEqualTo("a")
    }

    @Test
    fun `when sameUuidIsGenerated getNewLobby should returnUseNextUuid`() {
        val lobbyIdSlot = CapturingSlot<String>()
        val lobbyManager = LobbyManager(
            {
                mockk {
                    every { lobbyId = capture(lobbyIdSlot) } just Runs
                }
            },
            mockk {
                every { this@mockk.invoke() } returnsMany listOf("a", "a", "b")
            }
        )

        lobbyManager.createNewLobby()
        lobbyManager.createNewLobby()

        assertThat(lobbyIdSlot.captured).isEqualTo("b")
    }

    @Test
    fun `when sameUuidIsGenerated getAllLobbies should notReturnLobbyWithDuplicateId`() {
        val lobbyManager = LobbyManager(
            {
                mockk { every { lobbyId = any() } just Runs }
            },
            mockk {
                every { this@mockk.invoke() } returnsMany listOf("a", "a", "b")
            }
        )

        lobbyManager.createNewLobby()
        lobbyManager.createNewLobby()

        assertThat(lobbyManager.getAllLobbies()).hasSize(2)
    }

    @Test
    fun `findLobby should returnLobbyWithId`() {
        val lobbyManager = LobbyManager(
            { mockk { every { lobbyId = "a" } just Runs; every { lobbyId } returns "a" } },
            mockk { every { this@mockk.invoke() } returns "a" }
        )

        lobbyManager.createNewLobby()

        assertThat(lobbyManager.findLobby("a")).isNotNull()
    }

    @Test
    fun `when lobbyIdDoesntExist findLobby should returnNull`() {
        val lobbyManager = LobbyManager(
            { mockk { every { lobbyId = "a" } just Runs; every { lobbyId } returns "a" } },
            mockk { every { this@mockk.invoke() } returns "a" }
        )

        lobbyManager.createNewLobby()

        assertThat(lobbyManager.findLobby("b")).isNull()
    }
}