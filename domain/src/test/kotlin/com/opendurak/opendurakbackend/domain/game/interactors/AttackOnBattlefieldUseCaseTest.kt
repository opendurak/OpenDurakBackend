package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Card.Suit.*
import com.opendurak.opendurakbackend.domain.game.model.Card.Value.*
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.every
import io.mockk.mockk
import org.junit.Test

class AttackOnBattlefieldUseCaseTest {
    private val attacker = Player("Marvin", 0, mutableListOf(
        Card(SPADE, SIX)
    ))
    private val defender = Player("Alice", 1, mutableListOf(
        Card(SPADE, SEVEN)
    ))
    private val battlefield = Battlefield()
    private val game = mockk<Game> {
        every { this@mockk.attacker } returns this@AttackOnBattlefieldUseCaseTest.attacker
        every { this@mockk.defender } returns this@AttackOnBattlefieldUseCaseTest.defender
        every { this@mockk.helper } returns null
        every { this@mockk.battlefield } returns this@AttackOnBattlefieldUseCaseTest.battlefield
    }

    @Test
    fun `execute should addCardToBattlefield`() {
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
        assertThat(battlefield.getAttackingCards()).containsExactly(Card(SPADE, SIX))
    }

    @Test
    fun `execute should removeCardFromAttacker`() {
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
        assertThat(attacker.cards).isEmpty()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when playerUnknown execute should throwException`() {
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), mockk(), game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when defenderWantsToAttack execute should throwException`() {
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SEVEN), defender, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when attackerDoesntHaveCard execute should throwException`() {
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SEVEN), attacker, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasSameNumberOfStacksAsDefender execute should throwException`() {
        battlefield.addAttackingCard(mockk())
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
    }

    @Test
    fun `when battlefieldHasADefendedCardAndAttackerHasSameValue execute should addCardToBattlefield`() {
        val attackingCard = Card(DIAMOND, SIX)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(DIAMOND, SEVEN), attackingCard)
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
        assertThat(battlefield.getAttackingCards()).hasSize(2)
    }

    @Test
    fun `when battlefieldHasADefendedCardAndDefenderHasSameValue execute should addCardToBattlefield`() {
        val attackingCard = Card(HEART, SEVEN)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(DIAMOND, SIX), attackingCard)
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
        assertThat(battlefield.getAttackingCards()).hasSize(2)
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasCardsAndAttackingCardHasDifferentValue execute should throwException`() {
        val attackingCard = Card(HEART, SEVEN)
        battlefield.addAttackingCard(attackingCard)
        battlefield.addDefendingCard(Card(HEART, EIGHT), attackingCard)
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when battlefieldHasSixStacks execute should throwException`() {
        repeat(10) { defender.cards.add(mockk()) }
        repeat(6) { battlefield.addAttackingCard(mockk()) }
        AttackOnBattlefieldUseCase().execute(
            Card(SPADE, SIX), attacker, game,
        )
    }
}