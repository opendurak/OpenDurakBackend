package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.mockk
import org.junit.Test

class FillHandTo6CardsUseCaseTest {

    @Test
    fun `execute should fillCardsToSix`() {
        val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
        val player = Player("Marc", 0, mutableListOf())
        fillHandTo6CardsUseCase.execute(player, createDeck())
        assertThat(player.cards).hasSize(6)
    }

    @Test
    fun `execute should removeCardsFromStack`() {
        val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
        val deck = createDeck()
        fillHandTo6CardsUseCase.execute(Player("Marc", 0, mutableListOf()), deck)
        assertThat(deck).hasSize(4)
    }

    @Test
    fun `when notEnoughCardsInDeck execute should emptyStack`() {
        val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
        val deck = createDeck(4)
        fillHandTo6CardsUseCase.execute(Player("Marc", 0, mutableListOf()), deck)
        assertThat(deck).hasSize(0)
    }

    @Test
    fun `when notEnoughCardsInDeck execute should getAllCards`() {
        val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
        val player = Player("Marc", 0, mutableListOf())
        fillHandTo6CardsUseCase.execute(player, createDeck(4))
        assertThat(player.cards).hasSize(4)
    }

    @Test
    fun `when stackEmpty execute should doNothing`() {
        val fillHandTo6CardsUseCase = FillHandTo6CardsUseCase()
        val player = Player("Marc", 0, mutableListOf())
        fillHandTo6CardsUseCase.execute(player, mutableListOf())
        assertThat(player.cards).hasSize(0)
    }

    private fun createDeck(number: Int = 10): MutableList<Card> {
        val cards = mutableListOf<Card>()
        repeat(number) { cards.add(mockk()) }
        return cards
    }
}