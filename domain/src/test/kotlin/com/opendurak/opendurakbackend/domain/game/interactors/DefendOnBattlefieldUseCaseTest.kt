package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Card.Suit.SPADE
import com.opendurak.opendurakbackend.domain.game.model.Card.Value.*
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.every
import io.mockk.mockk
import org.junit.Test

class DefendOnBattlefieldUseCaseTest {
    private val attacker = Player("Marvin", 0, mutableListOf())
    private val defender = Player("Alice", 1, mutableListOf(
        Card(SPADE, SEVEN)
    ))
    private val attackingCard = Card(SPADE, EIGHT)
    private val battlefield = Battlefield().apply {
        addAttackingCard(attackingCard)
    }
    private val game = mockk<Game> {
        every { this@mockk.attacker } returns this@DefendOnBattlefieldUseCaseTest.attacker
        every { this@mockk.defender } returns this@DefendOnBattlefieldUseCaseTest.defender
        every { this@mockk.helper } returns null
        every { this@mockk.battlefield } returns this@DefendOnBattlefieldUseCaseTest.battlefield
        every { this@mockk.trump } returns mockk()
    }
    private val doesCardBeatUseCase = mockk<DoesCardBeatUseCase>()

    private val defendOnBattlefieldUseCase = DefendOnBattlefieldUseCase(doesCardBeatUseCase)

    init {
        assumeCardIsStronger()
    }

    @Test
    fun `execute should addCardToBattlefield`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, defender, game,
        )
        assertThat(battlefield.getDefendingCard(attackingCard)).isEqualTo(Card(SPADE, SEVEN))
    }

    @Test
    fun `execute should removeCardFromDefender`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, defender, game,
        )
        assertThat(defender.cards).isEmpty()
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when playerUnknown execute should throwException`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, mockk(), game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when attackerWantsToDefend execute should throwException`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, attacker, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when defenderDoesntHaveCard execute should throwException`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, NINE), attackingCard, defender, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when attackingCardDoesntExist execute should throwException`() {
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), Card(SPADE, TEN), defender, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when cardWasDefendedAlready execute should throwException`() {
        battlefield.addDefendingCard(mockk(), attackingCard)
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, defender, game,
        )
    }

    @Test(expected = IllegalArgumentException::class)
    fun `when cardIsWeaker execute should throwException`() {
        assumeCardIsWeaker()
        defendOnBattlefieldUseCase.execute(
            Card(SPADE, SEVEN), attackingCard, defender, game,
        )
    }

    private fun assumeCardIsStronger() {
        every { doesCardBeatUseCase.execute(any(), any(), any()) } returns true
    }

    private fun assumeCardIsWeaker() {
        every { doesCardBeatUseCase.execute(any(), any(), any()) } returns false
    }
}