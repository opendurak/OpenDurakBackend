package com.opendurak.opendurakbackend.domain.game.interactors

import com.google.common.truth.Truth.assertThat
import com.opendurak.opendurakbackend.domain.game.model.Battlefield
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.domain.game.model.Game
import com.opendurak.opendurakbackend.domain.game.model.Player
import io.mockk.mockk
import io.mockk.verify
import org.junit.Test

class ProgressRoundUseCaseTest {
    private val fillHandTo6CardsUseCase = mockk<FillHandTo6CardsUseCase>(relaxUnitFun = true)
    private val progressRoundUseCase = ProgressRoundUseCase(fillHandTo6CardsUseCase)
    private val attacker = Player("Alice", 1, mutableListOf(mockk()))
    private val defender = Player("Bob", 3, mutableListOf(mockk()))
    private val helper = Player("Cedric", 4, mutableListOf(mockk()))

    @Test
    fun `when offendersGaveUp execute should notAddCardsToDefender`() {
        val game = createGameWithOffendersGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(defender.cards).hasSize(1)
    }

    @Test
    fun `when defenderHadNoMoreCards execute should notAddCardsToDefender`() {
        defender.cards.clear()
        val game = createDefendingGame()
        progressRoundUseCase.execute(game)
        assertThat(defender.cards).hasSize(0)
    }

    @Test
    fun `when defenderGaveUp execute should addCardsToDefender`() {
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(defender.cards).hasSize(2)
    }

    @Test
    fun `when battlefieldIsFull execute should notAddCardsToDefender`() {
        val game = createGameWithFullBattlefield()
        progressRoundUseCase.execute(game)
        assertThat(defender.cards).hasSize(1)
    }

    @Test
    fun `when offendersGaveUp execute should clearBattlefield`() {
        val game = createGameWithOffendersGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.battlefield.getNumberOfStacks()).isEqualTo(0)
    }

    @Test
    fun `when defenderGaveUp execute should clearBattlefield`() {
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.battlefield.getNumberOfStacks()).isEqualTo(0)
    }

    @Test
    fun `execute should fillPlayerCardsBeforeRemovingPlayersWithoutCards`() {
        helper.cards.clear()
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        verify(exactly = 3) { fillHandTo6CardsUseCase.execute(any(), any()) }
    }

    @Test
    fun `when twoPlayersStillPlayingAndDefenderDidNotDefend execute should rotatePlayerRolesCorrectly`() {
        helper.cards.clear()
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.attacker).isEqualTo(attacker)
        assertThat(game.defender).isEqualTo(defender)
        assertThat(game.helper).isNull()
    }

    @Test
    fun `when twoPlayersStillPlayingAndDefenderDidDefend execute should rotatePlayerRolesCorrectly`() {
        helper.cards.clear()
        val game = createGameWithOffendersGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.attacker).isEqualTo(defender)
        assertThat(game.defender).isEqualTo(attacker)
        assertThat(game.helper).isNull()
    }

    @Test
    fun `when threePlayersStillPlayingAndDefenderDidNotDefend execute should rotatePlayerRolesCorrectly`() {
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.attacker).isEqualTo(helper)
        assertThat(game.defender).isEqualTo(attacker)
        assertThat(game.helper).isEqualTo(defender)
    }

    @Test
    fun `when threePlayersStillPlayingAndDefenderDidDefend execute should rotatePlayerRolesCorrectly`() {
        val game = createGameWithOffendersGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.attacker).isEqualTo(defender)
        assertThat(game.defender).isEqualTo(helper)
        assertThat(game.helper).isEqualTo(attacker)
    }

    @Test
    fun `when onePlayersIsLeft execute should setGameFinishedToTrue`() {
        attacker.cards.clear()
        helper.cards.clear()
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.gameFinished).isTrue()
    }

    @Test
    fun `when noPlayersIsLeft execute should setGameFinishedToTrue`() {
        attacker.cards.clear()
        helper.cards.clear()
        defender.cards.clear()
        val game = createDefendingGame()
        progressRoundUseCase.execute(game)
        assertThat(game.gameFinished).isTrue()
    }

    @Test
    fun `execute should removePlayersWithoutCards`() {
        attacker.cards.clear()
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.players).doesNotContain(attacker)
    }

    @Test
    fun `execute should setGiveUpStatesToFalse`() {
        val game = createGameWithDefenderGaveUp()
        progressRoundUseCase.execute(game)
        assertThat(game.attackerGaveUp).isFalse()
        assertThat(game.defenderGaveUp).isFalse()
        assertThat(game.helperGaveUp).isFalse()
    }

    @Test
    fun `when roundNotFinished execute should doNothing`() {
        val game = createDefendingGame()
        progressRoundUseCase.execute(game)
        assertThat(game.battlefield.getNumberOfStacks()).isEqualTo(1)
    }

    private fun createGameWithFullBattlefield(): Game {
        return Game(
            players = mutableListOf(this.attacker, this.defender, this.helper),
            stack = mutableListOf(),
            trump = mockk(),
            Battlefield(buildList<Pair<Card, Card?>> {
                repeat(6) { add(Pair(mockk(), mockk())) }
            }.toMap().toMutableMap()),
            attacker, defender, helper,
            attackerGaveUp = false, defenderGaveUp = false, helperGaveUp = false,
            gameFinished = false
        )
    }

    private fun createGameWithDefenderGaveUp(): Game {
        return Game(
            players = mutableListOf(this.attacker, this.defender, this.helper),
            stack = mutableListOf(),
            trump = mockk(),
            Battlefield(mutableMapOf(mockk<Card>() to null)),
            attacker, defender, helper,
            attackerGaveUp = false, defenderGaveUp = true, helperGaveUp = false,
            gameFinished = false
        )
    }

    private fun createGameWithOffendersGaveUp(): Game {
        return Game(
            players = mutableListOf(this.attacker, this.defender, this.helper),
            stack = mutableListOf(),
            trump = mockk(),
            Battlefield(mutableMapOf(mockk<Card>() to mockk())),
            attacker, defender, helper,
            attackerGaveUp = true, defenderGaveUp = false, helperGaveUp = true,
            gameFinished = false
        )
    }

    private fun createDefendingGame(): Game {
        return Game(
            players = mutableListOf(this.attacker, this.defender, this.helper),
            stack = mutableListOf(),
            trump = mockk(),
            Battlefield(mutableMapOf(mockk<Card>() to mockk())),
            attacker, defender, helper,
            attackerGaveUp = false, defenderGaveUp = false, helperGaveUp = false,
            gameFinished = false
        )
    }
}