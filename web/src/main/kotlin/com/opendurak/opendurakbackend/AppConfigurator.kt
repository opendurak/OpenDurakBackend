package com.opendurak.opendurakbackend

import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakbackend.api.LobbiesApi
import io.ktor.http.*
import io.ktor.serialization.jackson.*
import io.ktor.server.application.*
import io.ktor.server.plugins.callloging.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import io.ktor.server.plugins.defaultheaders.*
import io.ktor.server.plugins.statuspages.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import org.slf4j.LoggerFactory

class AppConfigurator(
    private val objectMapper: ObjectMapper,
    private val lobbiesApi: LobbiesApi,
) {
    private val logger = LoggerFactory.getLogger(this::class.simpleName)

    fun configure(application: Application): Unit = with(application) {
        install(StatusPages) {
            exception<IllegalArgumentException> { call, e ->
                call.respond(HttpStatusCode.BadRequest)
                logger.info("IllegalArgumentException: ${e.localizedMessage}")
                throw e
            }
            exception<IllegalStateException> { call, e ->
                call.respond(HttpStatusCode.BadRequest)
                logger.info("IllegalStateException: ${e.localizedMessage}")
                throw e
            }
            exception<NullPointerException> { call, e ->
                call.respond(HttpStatusCode.BadRequest)
                logger.info("NullPointerException: ${e.localizedMessage}")
                throw e
            }
            exception<Exception> { call, e ->
                call.respond(HttpStatusCode.InternalServerError)
                logger.info("Exception: ${e.localizedMessage}")
                throw e
            }
        }
        install(DefaultHeaders)
        install(CallLogging)
        install(WebSockets)
        install(CORS){
            anyHost()
            allowMethod(HttpMethod.Get)
            allowMethod(HttpMethod.Post)
            allowMethod(HttpMethod.Put)
            allowMethod(HttpMethod.Delete)
        }

        install(ContentNegotiation) {
            register(ContentType.Application.Json, JacksonConverter(objectMapper))
        }

        routing {
            route("lobby") { lobbiesApi.route(this) }

            get("/alive") {
                call.respond(HttpStatusCode.OK, "Durak is alive")
            }
        }
    }

}