package com.opendurak.opendurakbackend

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonMapperBuilder
import com.opendurak.opendurakbackend.api.GameApi
import com.opendurak.opendurakbackend.api.LobbiesApi
import com.opendurak.opendurakbackend.api.LobbyApi
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import java.security.MessageDigest

fun main() {
    val objectMapper = Injector.objectMapper
    val lobbiesApi = Injector.lobbiesApi
    embeddedServer(Netty, port = 8080) {
        AppConfigurator(objectMapper, lobbiesApi)
            .configure(this)
    }.start(wait = true)
}

private object Injector {
    val objectMapper: ObjectMapper = jacksonMapperBuilder().apply {
        configure(SerializationFeature.INDENT_OUTPUT, true)
    }.build()

    private val domainInjector = com.opendurak.opendurakbackend.domain.Injector(
        MessageDigest.getInstance("MD5"),
    )

    private val lobbyManager = domainInjector.lobbyManager
    private val gameApiFacade = domainInjector.gameApiFacade

    private val gameApi = GameApi(
        objectMapper,
        gameApiFacade,
    )
    private val lobbyApi = LobbyApi(gameApi, lobbyManager, objectMapper)
    val lobbiesApi = LobbiesApi(lobbyApi, lobbyManager, objectMapper)
}