package com.opendurak.opendurakbackend.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.opendurak.opendurakapi.DefendRequest
import com.opendurak.opendurakbackend.CommunicationChannelImpl
import com.opendurak.opendurakbackend.domain.game.GameApiFacade
import com.opendurak.opendurakbackend.domain.game.model.Card
import com.opendurak.opendurakbackend.getLobbyId
import com.opendurak.opendurakbackend.getUserId
import com.opendurak.opendurakbackend.mapper.CardMapper.mapToDomain
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import io.ktor.websocket.*

class GameApi(
    private val objectMapper: ObjectMapper,
    private val gameApiFacade: GameApiFacade,
) {

    fun route(route: Route): Unit = with(route) {
        webSocket("listenGameState") {
            val lobby = try {
                gameApiFacade.getLobby(call.getLobbyId())
            } catch (e: Exception) {
                return@webSocket close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "Lobby not found"))
            }
            val userId = try {
                call.getUserId()!!.also {
                    gameApiFacade.requireUserIsInLobby(it, lobby)
                }
            } catch (e: Exception) {
                return@webSocket close(CloseReason(CloseReason.Codes.PROTOCOL_ERROR, "UserId missing"))
            }

            val gameStateCommunicationChannel = CommunicationChannelImpl(this, objectMapper)
            lobby.subscribeToGameState(userId, gameStateCommunicationChannel)
            closeReason.await()
            lobby.unsubscribeToGameState(userId, gameStateCommunicationChannel)
        }

        post("start") {
            gameApiFacade.start(context.getLobbyId(), context.getUserId()!!)
            this.call.respond(HttpStatusCode.OK)
        }

        post("attack") {
            val card = objectMapper.readValue<Card>(this.call.receiveText())
            gameApiFacade.attack(card, context.getLobbyId(), context.getUserId()!!)
            this.call.respond(HttpStatusCode.OK)
        }

        post("help") {
            val card = objectMapper.readValue<Card>(this.call.receiveText())
            gameApiFacade.help(card, context.getLobbyId(), context.getUserId()!!)
            this.call.respond(HttpStatusCode.OK)
        }

        post("defend") {
            val cards = objectMapper.readValue<DefendRequest>(this.call.receiveText())
            gameApiFacade.defend(
                cards.defendingCard.mapToDomain(), cards.attackingCard.mapToDomain(),
                context.getLobbyId(), context.getUserId()!!
            )
            this.call.respond(HttpStatusCode.OK)
        }

        post("giveUp") {
            gameApiFacade.giveUp(context.getLobbyId(), context.getUserId()!!)
            this.call.respond(HttpStatusCode.OK)
        }
    }

}