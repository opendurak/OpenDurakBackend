package com.opendurak.opendurakbackend.api

import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakbackend.CommunicationChannelImpl
import com.opendurak.opendurakbackend.domain.lobby.LobbyManager
import com.opendurak.opendurakbackend.getLobbyId
import com.opendurak.opendurakbackend.getUserId
import com.opendurak.opendurakbackend.getUserName
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.websocket.*
import io.ktor.websocket.*

class LobbyApi(
    private val gameApi: GameApi,
    private val lobbyManager: LobbyManager,
    private val objectMapper: ObjectMapper,
) {

    fun route(route: Route): Unit = with(route) {
        route("game") { gameApi.route(this) }

        post("join") {
            val lobby = lobbyManager.findLobby(call.getLobbyId())
                ?: return@post this.call.respond(HttpStatusCode.BadRequest, "Lobby does not exist")
            val userName = call.getUserName()
                ?: return@post this.call.respond(HttpStatusCode.BadRequest, "Username missing")

            val userId = lobby.joinLobby(userName)
            val userIdJson = objectMapper.writeValueAsString(userId)
            context.respondText(userIdJson, ContentType.Application.Json, HttpStatusCode.OK)
        }

        webSocket("listenLobby") {
            val lobby = lobbyManager.findLobby(call.getLobbyId())
                ?: return@webSocket close(CloseReason(CloseReason.Codes.CANNOT_ACCEPT, "Lobby does not exist"))
            val userId = call.getUserId()
                ?: return@webSocket close(CloseReason(CloseReason.Codes.PROTOCOL_ERROR, "User Id missing"))

            val lobbyCommunicationChannel = CommunicationChannelImpl(this, objectMapper)
            lobby.subscribeLobby(userId, lobbyCommunicationChannel)
            closeReason.await()
            lobby.unsubscribeToLobby(userId, lobbyCommunicationChannel)
        }
    }
}