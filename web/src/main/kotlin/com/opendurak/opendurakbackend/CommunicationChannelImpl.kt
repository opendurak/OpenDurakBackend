package com.opendurak.opendurakbackend

import com.fasterxml.jackson.databind.ObjectMapper
import com.opendurak.opendurakbackend.domain.lobby.CommunicationChannel
import com.opendurak.opendurakbackend.domain.lobby.model.GameState
import com.opendurak.opendurakbackend.domain.lobby.model.UserId
import com.opendurak.opendurakbackend.mapper.GameStateMapper.mapToApi
import io.ktor.server.websocket.*
import io.ktor.websocket.*
import org.slf4j.LoggerFactory.getLogger

class CommunicationChannelImpl(
    private val webSocketServerSession: DefaultWebSocketServerSession,
    private val objectMapper: ObjectMapper,
) : CommunicationChannel {
    private val logger = getLogger(this::class.simpleName)

    override suspend fun send(content: GameState?) {
        sendAny(content?.mapToApi())
    }

    override suspend fun send(content: Collection<String>) {
        sendAny(content.toList())
    }

    override suspend fun send(content: UserId) {
        sendAny(content)
    }

    private suspend fun sendAny(content: Any?) {
        try {
            val jsonParameter = objectMapper.writeValueAsString(content)
            webSocketServerSession.send(jsonParameter)
        } catch (_: Exception) {
            logger.info("Could not send command to a player")
        }
    }

    override suspend fun close() {
        webSocketServerSession.send(Frame.Close())
    }
}