plugins {
    application
}

group = "com.opendurak.opendurakbackend"
version = "1.0"

dependencies {
    implementation(kotlin("stdlib"))

    implementation(project(":domain"))
    implementation("com.opendurak:OpenDurakApi:1.0.0-SNAPSHOT")

    implementation("ch.qos.logback:logback-classic:1.5.6")

    val ktorVersion = "2.3.10"
    implementation("io.ktor:ktor-serialization-jackson:$ktorVersion")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-websockets:$ktorVersion")
    implementation("io.ktor:ktor-server-call-logging:$ktorVersion")
    implementation("io.ktor:ktor-server-status-pages:$ktorVersion")
    implementation("io.ktor:ktor-server-content-negotiation:$ktorVersion")
    implementation("io.ktor:ktor-server-cors:$ktorVersion")
    implementation("io.ktor:ktor-server-default-headers:$ktorVersion")

    testImplementation("junit:junit:4.13.2")
}

application {
    mainClass.set("com.opendurak.opendurakbackend.ServerKt")
}
